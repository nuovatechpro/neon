<?php

// Importação das classes
use \Neon\Core\Neon;

// Importação do autoload do projeto
require __DIR__ . "/vendor/autoload.php";

// Definição da TIMEZONE
Neon::timezone('America/Bahia');

// Método de inicialização da aplicação
Neon::start(__DIR__, 'metro');
