# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
Here we write upgrading notes for brands. It's a team effort to make them as
straightforward as possible.

## [0.3.4.20220831] - 2022-08-31 11:13

### Add

- [app/Core/Http/CURL.php] : Criada classe para requisição via CURL

### Changed

- [app/Core/Entity.php] : Removida propriedade ID para permitir compatibilidade com outros sistemas.

## [0.3.3.20220811] - 2022-08-11 12:00

### Changed

- [app/Core/Http/Middleware/CORS.php] : Fixado para carregar os arquivos individuais de cada aplicação.


## [0.3.2.20220712] - 2022-07-12 10:25

### Changed

- [public\assets\css\widgets\Grid.css] :
  - Atualizado dispositivo da mediaquery.


## [0.3.2.20220708] - 2022-07-08 10:54

### Changed

- [app\Core\View.php] :
  - Predefinido valor para o parâmetro do método View::css() para ''.

- [public\asssets\css\widgets\Dialog.css] :
  - Alterado espaçamento na estrutura da janela de diálogo.

- [public\asssets\css\widgets\Dialog.css] :
  - Alterada propriedade display classe "row" para "flex" e adicionado gap;

## [0.3.1.20220209] - 2022-02-07 12:34

### Changed

- [public\assets\css\_root.scss] :

    - Adiciondas variáveis para controle da fonte da janela de diálogo e da cabeçalho da janela.

- [public\assets\css\core.css] :

    - Atualizada carga das regras por dependência.

- [public\assets\css\Dialog.css] :

    - Adicionadas variáveis css para controle da fonte e cor.

## [0.3.0.20220207] - 2022-02-07 11:10

### Add

- [public\assets\css\pages\sample\login]:
  
  - Aplicada regras css para renderização da tela.

- [public\assets\css\pages\sample\logout]:

  - Aplicação de regras css para renderização da tela.

- [public\assets\script\sample\login.js]
  
  - Aplicado script de gerenciamento do formulári de login.

- [public\assets\script\sample\logout.js]

  - Aplicado script de gerenciamento do formulári de logout.

- [public\pages\example\session-login.php] :
  
  - Implementado modelo de página pra login.
  
- [public\pages\example\session-logout.php]

  - Implementado modelo de página para logout.

- [public\pages\status\error-layout.php]

  - Implementado layout de página de erro.

- [public\pages\status\error-view.php]

  - Iplementada página de visualização de erros do sistema.








### Changed

- [app\Controller\Sample.php] :
  - Atualizada passagem dos parâmetros dos métodos View::template e View::render;
  - Implemento controle de sessão  no método session();
  - Implementado método de logout na classe.

- [app\Core\Connection.php] :

  - Implementados métodos delete(), insert() e update();

- [app\Core\Neon.php] : 
  - Removida chamada do método Neon::hello();
  - Adicionado carregamento de variáveis de ambiente no método Neon::start();
  - Acicionada validação de sessão no método Neon::start()


- [app\Core\Session.php] : 
  - Implementado método Session::getBody(), para retornar o corpo da sessão;
  - Alterada ordem da passagem de parâmetros no método Neon::start();
  - Remoção da alteração do session_name();

- [ap\Core\View.php] :

 - Removida variável page, não utilizada;

- [app\Routes\Sample.php] :
  - Adicionadas rotas de logout;


## [0.3.0.20220203] - 2022-02-03 21:20

### Add

- [app\Core\Controller\Sample.php] :
  
  -  Implementado controlador para gerenciar as demonstrações do framework;

- [public\assets\css_roots.css] :
  
  -  Carregamento das variáveis css;

- [public\assets\core.css] :
  
  -  Implementada regras gerais da aplicação para o layout;

- [public\assets\example\home.css] :
  
  -  Implementada regras gerais da página de exemplo de home;

- [public\assets\example\session.css] :
  
  -  Implementada regras gerais da página de exemplo de sessão;

- [public\assets\example\session.css] :
  
  -  Implementado layout para aplicação;

### Changed

- [app\Core\Neon.php] :

  - Amplida inclusão das rota para aplicação por meio da importação direta do arquivo via diretório;

  - Adicionado método start(), para iniciar o framework. O método já contempla a antiga chamada Neon::route();

- [app\Core\Session.php] :

  - Inclusão dos métodos de utilização da sessão;

- [app\Core\View.php] :  

  - Inclusão de variáveis para controle das visões;

  - Melhorada descrição de alguns métodos;

  - Alterada associação das variáveis de view para variáveis de controle global da classe VIEW;

- [public\assets\css\widgets\Buttons.scss] :

  - Alterada chamada da classes de 'btn' para 'neon-btn';

  - Alteração nas variáveis de cores dos botões de ações sistêmica para carregamento de arquivo de variáveis.

- [public\assets\css\widgets\Forms.scss] :

  - Removido trecho de código que não estava sendo utilizado;

  - Implementada página de visualização de erros.

### Deleted

- [public\assets\css\pages\example.css]

  - Excluída regra css para ser fundida com o core.scss.

- [public\assets\css\pages\example\index.php]:

  - Página excluída para dar lugar ao layout.php

### Fixed

- [public\assets\css\widgets\Buttons.scss] :
  - Corrigido erro de associação das regras em elementos input[type='button'] e input[type='submit']

## [0.2.1.202202011909] - 2022-02-01 19:09

### Changed
 - [app\Core\Neon.php]: Fixada criação de constante por importação do environment;

## [0.2.1.202202011626] - 2022-02-01 16:27

### Changed
  - [public\assets\css\widgets\Tabs.scss]: Definido tamanho padrão para fonte dos botões das abas.

## [0.2.1.202202011602] - 2022-02-01 16:02

### Changed
  - Alterado namespace de APP para Neon

## [0.2.0.202201281239] - 2022-01-27 12:39

### Added
  - [app\Config\config.json] : Adicionado arquivo contendo um array com a lista de origens permitidas par CORS;

### Changed
  - [app\Config\origin.php]: Removido arquivo php;
  - [app\Core\Http\Middleware\CORS.php]: Aplicada vericação por carga de arquivo origin.json;

## [0.2.0.20220128] - 2022-01-27 12:10

### Added
  - [app\Config.php] : Adicionado arquivo contendo um array com a lista de origens permitidas par CORS;
  - [app\Core\Http\Middleware\CORS.php] : Implementada classe CORS para gerenciamento de recursos em requisições externas;

### Changed
  - [composer.json]: Alterada chamada do namespase de "APP" para "Neon";
  - [app\Controller\*]: Alterada chamada de utilização do namespace de "APP" para "Neon";
  - [app\Route\*]: Alterada chamada de utilização do namespace de "APP" para "Neon";

## [0.1.11.20221027] - 2022-01-27 19:03

### Fixed
  - [app\Core\Http\Cors.php]: Chamada do header em Cors.

## [0.1.11.20221027] - 2022-01-27 15:10

### Fixed
  - Falha de timezone ao carregar a aplicação para o servidor.

### Changed
  - [index.php]: Adicionada chamada de timezone;

## [0.1.10.20220127] - 2022-01-27 08:06

### Changed
  - [public\assets\css\widgets\Window.css]: Alterado tamanho mínimo da altura do window__header.

## [0.1.10.20220127] - 2022-01-27 08:06

### Added
  - [app\Core\Http\Cors.php]: Implementada classe de gestão dos cabeçalhos de gerenciamento de recursos de CROSS-ORIGINS

### Changed
  - [app\Config\Connection.php]: Removida classe antiga de conexão com o bnanco de dados.
  - [app\Core\View.php]: Alterado caminho de carregamento do arquivo retirando 'Nuovatech' do caminho especificado;

## [0.1.9] - 2022-01-27 08:06

### Added
  - [public\assets\css\widgets\Tabs.css]: Adicionada regra css para Abas;

### Changed
  - [public\assets\css\widgets\Window.css]: Adicionada regra css para Abas;
  
### Fixed
- [public\assets\script\Modal.js]: Adicionado trecho no script de renderização de página por meio do innerHTMl permitindo o funcionamento de script;

## [0.1.8] - 2022-01-03 11:52

### Added
 - [public\assets\css\widgets\Cheacks.css]: Implementado css para customização dos input radio;
 - [public\assets\script\Mask.js]: Implementado Jquery.Mask.js ao projeto para utilizar máscaras nos campos de formulário;
 - [vendor/igorescobar]: Importado projeto Jquery.Mask.js de igorescobar;

### Changed
  - [App\Core\Neon.php]: Adicionada chamada do método run() ao objeto de rastreamento de rotas no método Neon::route();
  - [App\Core\Neon.php]: Implementado método Neon::response() de resposta para objetos em json;
  - [App\Core\View.php]: Adicionada variável e $vars, para conter as variáveis que serão importadas para a visão;
  - [App\Roues\Pages.php]: Removida chamado do método run do objeto de rotas;
  - [Index.php]: Alterado carregamento das rotas da apicação e variáveis de ambiente para métodos da classe Neon::url e Neon::route();


## [0.1.7] - 2022-01-03 11:52

### Added
 - [Connection.php]: Carregada classe de conexão com o banco de dados;
 - [Entity.php]: Classe de mapeamento para classes modelo;
 - [Middleware.php]: Inicio da implementação de middleware;
 - [Dataset.scss]: Carregado widget css de dataset;
 - [Dialog.scss]: Carregado widget css de Dialog;
 - [Pages.scss]: Carregado widget css de Pages;
 - [Workspace.scss]: Carregado widget css de Workspace;
 - [Dialog.js]: Carregado script de gerenciamento dos DIalogs;
 - [Form.js]: Carregado script de gerenciamento dos formulários;
 - [Jquery.js]: Carregada biblioteca auxiliar jquery;

 ### Changed

 - [Neon.php]: Adicionado método route para controle das rotas e carregamento do arquivo PAGES das aplicações;

 - [Neon.php]: Adicionado método url para carregamento e definição da URL como constante da aplicação;

 - [Neon.php]: Adicionado método environment para carregamento de arquivo .env contendo as variáveis de ambiente da aplicação;

 - [Neon.php]: Alerado retorno do fluxo de importações quand a saída não é identificada;

 - [Neon.php]: Alerado retorno do fluxo de importações quand a saída não é identificada;

 ### Removed
 -[William-costa] - Removida importação via composer do componente Environment de Wiliam-Costa



## [0.1.6] - 2022-01-03 11:52

### Added

- [Loader.css]: Criada folha de estios para renderização do Loader;

### Changed

- [Core/Email.php]: Alterada passagem de parâmetro de "langcode: $params["LANG"]" para "$params["LANG"]" ;
- [Modal.css]: Adicionadas propriedades para comportar o Loader.css;

## [0.1.5] - 2021-12-31 19:03

### Added

- [env]: Criada lista de variáveis de ambiente;
- [Controller/Email]: Implementação controlador de envio de email para exemplo;
- [Core/Email]: Implementação de classe de gerenciamento de E-mail associada à importação de PHPMAILLER;

### Changed

- [Core/Http/Response.php]: Adicionado tratamento para json;
- [Routes/Pages.php]: Adicionadas novas rotas;


## [0.1.4] - 2021-12-30 12:25

### Added
- [erro.css]: Criada folha de estilo para a página de erros HTTP;
- [favicon]: Adicionado favicon da aplicação;
- [folder/Routes]: Pasta de Rotas criada;
- [PhpMailer]: Incorporado ao projeto;
- [Environment]: Incorporado ao projeto;

### Changed
- [index.php]: Reestruturada improtação das dependências de roteamento;
- [Router.php]: Implemementado mapeamento para controladores e tratamento das exceções;
- [example.css]: Melhoria na organiação da folha de estilo;
- [error.php]: Reorganiação da estrutura da página;

### Fixed
- [Router.php]: Fixado o erro ao carregar os parâmetros dinâmicos na URL;

## [0.1.3] - 2021-12-28 22:47

### Added
-  [Routes.php]: Fixado erro de chamada de método de classe não instanciada;
-  [example]: criada tela de exemplo

## [0.1.2] - 2021-09-17 11:19
- [Core.php]: Adicionado método degug para verificar os valores de um objeto ou array.

## [0.1.1] - 2021-09-17 02:39

### Changed
- Alterado .gitignore para não carregar a subpasta de temas do framework.

## [0.1.0] - 2021-09-17 02:30
  
Carregamento das dependencias iniciais do projeto do framework.
 
### Added
- Dependencias iniciais para o projeto, classes, modelos, persistência de acesso ao banco de dados, scripts, folhas de estilo.

