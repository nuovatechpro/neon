<?php

namespace Neon\Core;

use \Neon\Core\Neon;
use PDO;
use PDOException;

/**
 * @desc: classe de persistência ao banco de dados
 * @author: Eduardo Marinho
 * @version: 1.0.0.0;
 */
abstract class Connection
{
    private static function connect()
    {
        try {
            $connectionString =  DB_DRIVER . ':host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME . ';user=' . DB_USER . ';password=' . DB_PASS;
            $pdo = new PDO($connectionString);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (\PDOException $e) {
            throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
    }

    /**
     * @desc: executa uma query sem retornar valores
     * @param: $query String contendo a query;
     * @param: $params Array variável de auxílio para execução.
     * @return: boolean TRUE, caso tenha sido realizado alterações; FALSE, sem alteração.
     */
    public static function execute($query, $params = array())
    {
        $statement = self::connect()->prepare($query);
        $statement->execute($params);
        return ($statement->rowCount() > 0) ? true :  false;
    }

    /**
     * @desc: realiza a consulta no banco de dados e retorna valores
     * @param: $query String contendo a query;
     * @param: $params Array variável de auxílio para execução.
     */
    public static function select($query, $params = array())
    {
        $statement = self::connect()->prepare($query);
        $statement->execute($params);
        $data = $statement->fetchAll();
        return $data;
    }

    /**
     * Método de remoção de dados do banco de dados.
     * @param string $table tabela de referência para gerar a exclusão
     * @param string $key   campo que será realizado com base
     */
    public static function delete(string $table, string $key)
    {
    }

    /**
     * Método de remoção de insersão de dados no banco de dados.
     * @param  string $table 
     */
    public static function insert(string $table, string $key)
    {
    }

    /**
     * Método de atualização de dados nas tabelas do banco de dados.
     */
    public static function update(string $table, string $key)
    {
    }
}
