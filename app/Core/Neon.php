<?php

namespace Neon\Core;

use Neon\Core\Http\Router;

/**
 * Classe de gestão do Framework. Deve ser a primeira classe importada para o projeto
 * @author Eduardo Marinho
 * @since  18/06/2021
 * @version 0.1.2.20210917
 */
class Neon
{
    /**
     * Método responsável por importar os controladores da aplicação
     * @param string $class nome da classe que será carregada
     */
    private static function autoController(string $dir)
    {
         // Carrega conteúdo para o BUFFER
         ob_start();

         // Diretório dos arquivos de rotas
         $directory = dir($dir .  "/app/Controller/");
 
         // Percorre o diretório em busca dos mapas de rotas
         while ($file = $directory->read()) {
             if (stripos($file, 'php')) {
                 // Importa o arquivo de rotas.
                 require_once($dir . "/app/Controller/$file");
             }
         }
         // Limpa o conteúdo do BUFFER para evitar erros
         ob_end_flush();
    }

    /**
     * Método responsável por realizar o debug de objetos pelo tipo informado
     * @param array $object array para depuração
     * @param mixed $object um objeto para impressão
     * @param bool $stop indica se a execução do código será interrompida após o retorno.
     * @param string $type tipo de retorno esperado [json, html, array]. O retorno padrão será array;
     */
    public static function debug($object, bool $stop = null, string $type = null)
    {
        echo ($type == "json") ? json_encode($object) : "<pre>" . print_r($object, true) . "</pre>";

        if ($stop == true) {
            exit;
        }
    }

    /**
     * Método responsável por carregar as variáveis de ambiente do projeto
     * @param  string $dir Caminho absoluto da pasta onde encontra-se o arquivo .env
     * @author WilliamCosta
     * @see https://www.youtube.com/c/WDEVoficial/featured
     */
    public static function environment(string $dir)
    {
        //VERIFICA SE O ARQUIVO .ENV EXISTE
        if (!file_exists($dir . '/.env')) {
            return false;
        }

        //DEFINE AS VARIÁVEIS DE AMBIENTE
        $lines = file($dir . '/.env');
        foreach ($lines as $line) {
            $constant = explode('=', $line);
            define(trim($constant[0]), trim($constant[1]));
        }
    }

    /**
     * Método responsável por importar a classe de acordo com  o tipo desejado
     * @param string $type {config, core, controller, dao, interface, model}
     * @param string $class nome da classe carregadas
     */
    public static function import(string $type, string $class)
    {
        if (!empty($type) && !empty($class)) {

            $type = strtolower($type);

            switch ($type) {
                case "config"; {
                        require "app/Config/$class.php";
                        break;
                    }
                case "core"; {
                        require "app/Core/$class.php";
                        break;
                    }
                case "controller"; {
                        require "app/Controller/$class.php";
                        break;
                    }
                case "dao"; {
                        require "app/Dao/$class.php";
                        break;
                    }
                case "interface"; {
                        require "app/Interface/$class.php";
                        break;
                    }
                case "model"; {
                        require "app/Model/$class.php";
                        break;
                    }
                default: {
                        return "Tipo ou classe não encontrada!";
                        break;
                    }
            }
        } else {
            throw ("Não existem parâmetros definidos para importação!");
        }
    }

    /**
     * Método de importação de classes modelo
     * @param   string  $class nome da classe que será carregada.
     */
    public static function model(string $class)
    {
        require_once "app/Model/$class.php";
    }

    /**
     * Método responsável por iniciar as rotas para o projeto.
     * A definição das URL precisam já terrem sido carregadas antes da chamada deste método.
     * @param   string $dir diretório padrão do projeto
     */
    public static function route(string $dir)
    {

        // Carrega conteúdo para o BUFFER
        ob_start();

        // Variável que chamará as rotas do projeto
        $obRouter = new Router(URL);

        // Diretório dos arquivos de rotas
        $directory = dir($dir .  "/app/Routes/");

        // Percorre o diretório em busca dos mapas de rotas
        while ($file = $directory->read()) {
            if (stripos($file, 'php')) {
                // Importa o arquivo de rotas.
                require_once($dir . "/app/Routes/$file");
            }
        }

        // Executa a chamada da rotas
        $obRouter->run()->sendResponse();

        // Limpa o conteúdo do BUFFER para evitar erros
        ob_end_flush();
    }

    /**
     * Método de inicialização do framework.
     * @param   string  $dir diretório da aplicação.
     * @param   string  $template diretório do template externo.
     */
    public static function start(string $dir, string $template = null)
    {

        // Carrega as variáveis de ambiente
        self::environment($dir);

        // Carregamento do template caso tenha sido declarado
        if (!empty($template)) {
            self::template($template);
        }

        // Inicia a variável de sessão para utilização futura
        if (session_start() != PHP_SESSION_ACTIVE) {
            session_start();
        }

        // Carregamento dos controladores
        self::autoController($dir);

        // Realiza o carregamento das rotas
        self::route($dir);
    }

    /**
     * Método de carregamento do template externo
     * @param   string $name nome da pasta do template
     */
    public static function template(string $name)
    {
        $name = strtolower($name);
        require_once("public/templates/$name/Template.php");
    }

    /**
     * Método responsável por carregar o caminho inicial da URL já validando se o protocolo é HTTP ou HTTPS
     * @see     https://www.php.net/manual/pt_BR/timezones.php
     * @param   string $path complemento da URL
     */
    public static function url(string $path, bool $print = false)
    {
        $url = isset($_SERVER["HTTPS"]) ? "https://"  : "http://";
        $url .= $_SERVER["SERVER_NAME"] . '/' . $path;
        if ($print == true) {
            print_r($url);
        } else {
            return $url;
        }
    }

    /**
     * Método responsável por carregar o timezone para aplicação
     * @param   string  $place local da zona
     */
    public static function timezone(string $place)
    {
        date_default_timezone_set($place);
    }

    public function response(array $args = [])
    {
        echo json_encode($args);
    }

    private static function setup($dir)
    {
        require_once($dir . "/Neon/public/pages/setup/config.php");
    }
}
