<?php

namespace Neon\Core;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Email
{
    private $mail;

    public function __construct($params)
    {
        $this->mail = new PHPMailer(true);

        // Configuração do servidor
        $this->mail->isSMTP();
        $this->mail->isHTML();
        $this->mail->setLanguage($params["LANG"]);
        $this->mail->CharSet = $params["CHARSET"];

        // Configuração de segurança
        $this->mail->SMTPAuth   = $params["SMTP_AUTH"];
        $this->mail->SMTPSecure = isset($params["SMTP_SECURE"]) ? $params["SMTP_SECURE"] : PHPMailer::ENCRYPTION_SMTPS;

        // Configuração de autenticação
        $this->mail->Host       = isset($params["HOST"]) ? $params["HOST"] :  '';                //Set the SMTP server to send through
        $this->mail->Username   = isset($params["USERNAME"]) ? $params["USERNAME"] : '';         //SMTP username
        $this->mail->Password   = isset($params["PASSWORD"]) ? $params["PASSWORD"] : '';         //SMTP password
        $this->mail->Port       = isset($params["PORT"]) ? $params["PORT"] : '';                 //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        // Recipients
        $this->mail->setFrom(
            isset($params["SEND_FROM"]) ? $params["SEND_FROM"] : '',
            isset($params["SEND_FROM_NAME"]) ? $params["SEND_FROM_NAME"] : ''
        );
    }

    /**
     * Método de inclusão do conteúdo no corpo da mensagem
     * @param   string $subject Motivo da mensagem
     * @param   string $body Corpo da mensagem
     */
    public function content(string $subject = '', string $body = '', string $altBody = '')
    {
        // Content
        $this->mail->Subject = $subject;
        $this->mail->Body = $body;
        $this->mail->AltBody = $altBody;
    }

    public function recipient($sendTo = '', $sendToName = '')
    {
        $this->mail->addAddress($sendTo, $sendToName);
    }

    public function send()
    {
        try {
            $this->mail->send();
            return array(
                "msg" => "E-mail enviado com sucesso!",
                "status" => 200
            );
        } catch (Exception $e) {
            return array(
                "msg" => $e->getMessage(),
                "status" => $e->getCode()
            );
        }
    }
}
