<?php

namespace Neon\Core\Http;

use \Neon\Core\Neon;
use stdClass;

/**
 * Classe de gerenciamento das requisições via CURL
 */
abstract class CURL
{

    /**
     * @var mix
     * Corpo da requisição
     */
    public static $body;

    /**
     * @var array
     */
    public static $header = [];

    /**
     * @var string
     * URL da requisição
     */
    public static $url;

    /**
     * @var string
     * Método da requisição
     */
    public static $method = "POST";

    /**
     * @var int
     * Tempo máximo de execução da requisição
     */
    public static $timeout = 60;

    /**
     * Realiza o envio de uma requição ao servidor
     * @param stdClass $curlOpt opções da requisição
     * @return res
     */
    public static function send(stdClass $curlOpt)
    {
        // Realiza a carga dos atributos para a classe
        self::prepare($curlOpt);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => self::$timeout,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => self::$method,
            CURLOPT_POSTFIELDS => self::$body,
            CURLOPT_HTTPHEADER => self::$header
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    /**
     * Realiza o mapeamento das configurações e substitui os valores que foram definidos
     */
    private static function prepare(stdClass $curlOpt)
    {

        // define o body
        self::$body = (!empty($curlOpt)) ? $curlOpt->body : self::$body;

        // define o header
        self::$header = (!empty($curlOpt)) ? $curlOpt->header : self::$header;

        // define a url
        self::$url = (!empty($curlOpt)) ? $curlOpt->url : self::$url;
    }
}
