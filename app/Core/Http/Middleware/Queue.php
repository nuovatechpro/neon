<?php

namespace Neon\Core\Http\Middleware;

class Queue
{

    /**
     * Fila dos middlewares a serem executados
     * @var array
     */
    private $middlewares = [];

    /**
     * Médodo de execução do controlador
     * @var Clousure
     */
    private $controller;

    /**
     * Argumentos dos médotos do controlador
     * @var array
     */
    private $controllerArgs = [];

    /**
     * Método responsável por construir a classe de fila de middlewares
     * @param   array $middlewares
     * @param   Clousure $controller
     * @param   array $controllerArgs
     */
    public function __construct($middlewares, $controller, $controllerArgs)
    {
        $this->middlewares = $middlewares;
        $this->controller = $controller;
        $this->controllerArgs = $controllerArgs;
    }
}
