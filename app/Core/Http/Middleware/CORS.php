<?php

namespace Neon\Core\Http\Middleware;

use Neon\Core\Neon;
use stdClass;

/**
 * Classes para gerenciamento dos acessos via headers do servidor.
 * TODO
 * @since 27/01/2022
 * @author  Eduardo Marinho
 * @version 0.0.1.20220127
 */
abstract class CORS
{
    /**
     * Médodo de inclusão de cabeçalho de conteúdo por recurso
     * @param   string  $type tipo da mídia, valor padrão text/html
     * @param   string  $charset formato do chaset
     */
    static public function contentType(string $type = null, string $charset = "utf-8")
    {
        switch ($type) {
            case "json":
                $application = "application/json";
                break;

            default:
                $application = "text/html";
                break;
        }
        header('Content-type:' . $application . ';charset=' . $charset);
    }

    /**
     * Método responsável por verificar o permissionamento de solicitação para o recurso de uma origem
     * verificando se o mesmmo está presente na lista pré-definida no arquivo origin.ini da raíz do projeto.
     * Caso seja informada uma origem como parâmetro, a validação será realizada pelo parâmetro informado,
     * caso passe o valor true, o script tentará validar o mapeamento tanto pelo arquivo de origin.ini quanto
     * o valor informado em $requestOrigin;
     * 
     * @param   string  $requestOrigin URL que será utilizada para validação;
     * @param   boolean $compare habilita a verificação no arquivo de origin.php;
     * @return  array mensagem de permissão
     */
    static public function origin(string $requestOrigin, bool $compare = false)
    {
        $response = [];
        if ($compare === true) {

            // Caminho do arquivo origin.json
            $origins = $_SERVER["DOCUMENT_ROOT"] . $_SERVER["REQUEST_URI"] . "app/Config/origin.json";

            if (file_exists($origins)) {

                // Carregamento do origin.json
                $json = file_get_contents($origins);

                // Importa o arquivo oriign para a classe
                foreach (json_decode($json) as $origin) {
                    if ($origin === $requestOrigin) {
                        header("Access-Control-Allow-Origin: $requestOrigin");
                    }
                }
                $response["msg"] = "Origem permitida.";
                $response["status"] = 200;
            } else {
                $response["msg"] = "Origem da requisição não está na lista de permissões.";
                $response["status"] = 403;
            }
        } else {

            header("Access-Control-Allow-Origin: $requestOrigin");
            $response["msg"] = "Origem permitida.";
            $response["status"] = 200;
        }
        return $response;
    }
}
