<?php

namespace Neon\Core\Widgets;

abstract class Table
{
    static public function icon($url, $ext = "png")
    {
        $path = $_SERVER['REQUEST_SCHEME'] . "://" .  $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . "neon";
        $url = $path . "/public/assets/widgets/datatable/images/" . $url . '.' . $ext;
        echo "<img src='$url' height='18px' >";
    }
}
