<?php

namespace Neon\Core\Controller;

use Neon\Core\View;

abstract class Teste
{
    /**
     * Método de renderização da home page
     */
    static public function home()
    {
        View::render("example/home", [
            "site" => "Neon - Home"
        ]);
    }
}
