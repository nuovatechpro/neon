<?php

namespace Neon\Core\Controller;

abstract class Email
{

    static public function send()
    {
        // Array de configurações
        $config = array(
            "HOST" => "smtp.example.com",
            "USERNAME" => "example@example.com",
            "PASSWORD" => "secret",
            "PORT" => 587,
            "CHARSET" => "utf-8",
            "LANG" => "br",
            "SMTP_AUTH" => true,
            "SMTP_SECURE" => "TLS",
            "SEND_FROM" => "example@example.com",
            "SEND_FROM_NAME" => "Somebody",
        );

        // Instacia da classe EMAIL
        $mail = new \Neon\Core\Email($config);

        // Adição de destionatário
        $mail->recipient('sample@sample.com', "Someone");

        // Adição do contéudo
        $mail->content("Teste", "Teste de mensagem");

        // Envio do email
        return $mail->send();
    }
}
