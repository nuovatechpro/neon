<?php

namespace Neon\Core\Controller\Pages;

use Neon\Core\Neon;
use Neon\Core\Session;
use Neon\Core\View;

/**
 *  Classe contendo os exemplos de funções do framerwork
 *  @author Eduardo Marinho
 *  @copyright  Nuovatech
 */
abstract class Sample
{

    /**
     * Método de renderização da home page
     */
    static public function home()
    {
        View::render("example/home", [
            "site" => "Neon - Home"
        ]);
    }

    /**
     * Método de renderização da página de sessão
     */
    static public function session()
    {
        if (Session::check("nuovatech")) {
            // Renderização da página
            View::template("example/session-logout", [
                "site" => "Neon - Sessão Ativa"
            ]);
        } else {
            // Renderização da página
            View::template("example/session-login", [
                "site" => "Neon - Sessão Desativada"
            ]);
        }
    }

    /**
     * Método de login
     */
    static public function login($request)
    {
        $form = $request->getPostVars();
        if (!empty($form['login']) && !empty($form['password'])) {

            Session::start("nuovatech", $form);

            return array(
                "msg" => "Acesso concebido.",
                "status" => 200
            );
        } else {
            return array(
                "msg" => "Credênciais inválidas.",
                "status" => 403
            );
        }
    }

    /**
     * Método de Logout da aplicação
     */
    static public function logout()
    {
        try {
            Session::close();
            return array(
                "msg" => "Deslogado com sucesso.",
                "status" => 200
            );
        } catch (\Throwable $th) {
            return array(
                "msg" => $th->getMessage(),
                "status" => $th->getCode()
            );
        }
    }
}
