<?php

namespace Neon\Core\Interfaces;

interface Session
{
    public function login();

    public function logout();
}
