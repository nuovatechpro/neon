<?php

/**
 * Rotas de exemplos do sistema
 */

// use \Neon\Core\Neon;
// use \Neon\Core\Http\Response;
// use \Neon\Core\Controller\Pages\Sample;
// use Neon\Core\Http\Middleware\RequireLogout;
// use Neon\Core\Session;

// // ROTAS PARA SESSÃO DE EXEMPLO
// $obRouter->get('/session', [
//     function () {

//         Neon::import("controller", "Sample");
//         return new Response(200, Sample::session());
//     }
// ]);

// $obRouter->get('/session/logout', [
//     function () {
//         Neon::import("controller", "Sample");
//         return new Response(200, Sample::logout(), "application/json");
//     }
// ]);

// // ROTA PARA REALIZAR A SESSÃO
// $obRouter->post('/session', [
//     function ($request) {
//         Neon::import("controller", "Sample");
//         $valid = Sample::login($request);
//         return new Response($valid['status'], $valid, "application/json");
//     }
// ]);
