<?php

/**
 * Rotas principais do sistema
 */

// Importação dos cores
use \Neon\Core\Http\Response;
use \Neon\Core\Neon;
use \Neon\Core\View;

// Importação dos controladores
use \Neon\Core\Controller\Email;
use Neon\Core\Controller\Teste;
// use Neon\Core\Controller\Pages\Sample;
use Neon\Core\Session;

// Rota de Home
$obRouter->get(
    '/',
    [
        [
            "middlewares" => [
                "maintainence"
            ]
        ],
        function () {
            return new Response(200, Teste::home());
        }
    ]
);

// Rota de Error
$obRouter->get('/error', [
    function () {
        return new Response(200, View::render("status/error"));
    }
]);

// Rota de Dinâmica
$obRouter->get('/pagina/{idPagina}/{acao}', [
    function ($idPagina, $acao) {
        return new Response(200, $idPagina . " - " . $acao);
    }
]);

// Rota de teste de e-mail
$obRouter->get('/email', [
    function () {
        Neon::import("controller", "Email");
        return new Response(200, Email::send(), "application/json");
    }
]);
