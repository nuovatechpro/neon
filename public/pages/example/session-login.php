<?php

use \Neon\Core\View;

View::script('sample/session/login');

// Regras CSS
View::css('widgets/Forms');
View::css("pages/sample/login");
?>

<form class="form__model --plate" id="form-acesso">
    <div class="form__row">
        <fieldset class="col__12">
            <label for="login">Login:</label>
            <input id="login" name="login" type="text">
        </fieldset>
    </div>
    <div class="form__row">
        <fieldset class="col__12">
            <label for="password">Senha:</label>
            <input id="password" name="password" type="password">
        </fieldset>
    </div>

    <div class="form__row">
        <fieldset class="col__12">
            <input class="neon-btn --confirm btn__size --mid btn__shape --smooth" onclick="acessar();" type="button" value="Enviar">
        </fieldset>
    </div>
</form>