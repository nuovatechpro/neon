<?php

use \Neon\Core\Neon;
use \Neon\Core\Session;
use \Neon\Core\View;

View::css("widgets/Buttons");
View::css("pages/sample/logout");
View::script("sample/session/logout");
?>

<div class="card">
    <form onsubmit="return false;">

        <h1>Você está logado!</h1>
        <p>
            <strong>Usuário: </strong>
            <?php echo Session::getBody("nuovatech")['login']; ?>
        </p>
        <p>
            <strong>Senha: </strong>
            <?php echo Session::getBody("nuovatech")['password']; ?>
        </p>
        <button class="neon-btn --confirm btn__size --mid btn__shape --smooth" onclick="logout();" title="Realizar logout" type="submit">Sair</button>
    </form>
</div>