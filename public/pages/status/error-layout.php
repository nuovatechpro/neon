<?php

use \Neon\Core\Neon;
use \Neon\Core\View;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php echo View::$global['site']; ?></title>
    <?php View::css("pages/error", true); ?>
</head>

<body>
    <main>
        <div class="container">
            <h1><?php echo View::$global['status']; ?></h1>
            <h2><?php echo View::$global['msg']; ?></h2>
        </div>
    </main>
</body>

</html>