var Dialog = {

    // Key do dialog
    key: null,

    alert: function (msg, title = null, event = null) {
        if (msg == null) {
            console.warn("Dialog.alert()=> msg é nula ou está vazia!");
        } else {

            // Gera a key dinamicamente
            const keyContruct = "neon-dialog-";
            this.key = Date.now();

            // Construção da janela
            let dialog = document.createElement("div");
            dialog.classList.add("neon-dialog");
            dialog.id = keyContruct + this.key;

            //Dialog Header
            let dialogHeader = document.createElement("div");
            dialogHeader.classList.add("neon-dialog__header");
            dialogHeader.innerText = (title == null) ? "Atenção!" : title;
            dialog.append(dialogHeader);

            //Dialog Close
            let btnClose = document.createElement("button");
            btnClose.title = "Fechar Janela";
            btnClose.innerText = 'x';
            btnClose.type = "button";
            btnClose.value = this.key;
            if (event == null) {
                btnClose.addEventListener("click", function () {
                    Modal.close(this.value);
                });
            } else {
                btnClose.addEventListener("click", function () {
                    event();
                });
            }
            dialogHeader.append(btnClose);

            //Dialog Body
            let dialogBody = document.createElement("div");
            dialogBody.classList.add("neon-dialog__body");
            dialogBody.innerText = msg;
            dialog.append(dialogBody);

            //Dialog Footer
            let dialogFooter = document.createElement("div");
            dialogFooter.classList.add("neon-dialog__footer");
            dialog.append(dialogFooter);

            //Dialog Accept
            let btnAccept = document.createElement("button");
            btnAccept.classList.add("neon-dialog__btn");
            btnAccept.classList.add("--close");
            btnAccept.innerText = 'Fechar';
            btnAccept.title = "Fechar Janela";
            btnAccept.type = "button";
            btnAccept.value = this.key;
            if (event == null) {
                console.log("null");
                btnAccept.addEventListener("click", function () {
                    Modal.close(this.value);
                });
            } else {
                btnAccept.addEventListener("click", function () {
                    event();
                });
            }
            dialogFooter.append(btnAccept);

            // Incluí a janela no modal
            Modal.open(this.key, dialog);
        }
    },

    confirm: function (msg, confirmEvent, closeEvent = null) {
        if (msg == null || confirmEvent == null) {
            console.warn("Dialog.confirm()=> msg é nula ou está vazia!");
        } else {

            // Gera a key dinamicamente
            const keyContruct = "neon-confirm-";
            this.key = Date.now();

            // Construção da janela
            let dialog = document.createElement("div");
            dialog.classList.add("neon-dialog");
            dialog.id = keyContruct + this.key;

            //Dialog Header
            let dialogHeader = document.createElement("div");
            dialogHeader.classList.add("neon-dialog__header");
            dialogHeader.innerText = "Confirmar!";
            dialog.append(dialogHeader);

            //Dialog Close
            let btnClose = document.createElement("button");
            btnClose.innerText = 'x';
            btnClose.title = "Fechar Janela";
            btnClose.type = "button";
            btnClose.value = this.key;
            if (closeEvent == null) {
                btnClose.addEventListener("click", function () {
                    Modal.close(this.value);
                });
            } else {
                btnClose.addEventListener("click", function () {
                    closeEvent();
                });
            }
            dialogHeader.append(btnClose);

            //Dialog Body
            let dialogBody = document.createElement("div");
            dialogBody.classList.add("neon-dialog__body");
            dialogBody.innerText = msg;
            dialog.append(dialogBody);

            //Dialog Footer
            let dialogFooter = document.createElement("div");
            dialogFooter.classList.add("neon-dialog__footer");
            dialog.append(dialogFooter);

            //Dialog Reject
            let btnReject = document.createElement("button");
            btnReject.classList.add("neon-dialog__button", "--cancel");
            btnReject.innerText = 'Cancelar';
            btnReject.title = "Cancelar e fechar janela";
            btnReject.type = "button";
            btnReject.value = this.key;
            if (closeEvent == null) {
                btnReject.addEventListener("click", function () {
                    Modal.close(this.value);
                });
            } else {
                btnReject.addEventListener("click", function () {
                    closeEvent();
                });
            }
            dialogFooter.append(btnReject);

            //Dialog Accept
            let btnAccept = document.createElement("button");
            btnAccept.innerText = 'Confirmar';
            btnAccept.title = "Confirmar ação";
            btnAccept.type = "button";
            btnAccept.value = this.key;
            if (confirmEvent == null) {
                btnAccept.addEventListener("click", function () {
                    Modal.close(this.value);
                });
            } else {
                btnAccept.addEventListener("click", function () {
                    confirmEvent();
                });
            }
            dialogFooter.append(btnAccept);

            // Incluí a janela no modal
            Modal.open(this.key, dialog);
        }
    },

    close: function (key) {
        let dialog = document.getElementById("neon-modal-" + key);
        dialog.remove();
    }
}