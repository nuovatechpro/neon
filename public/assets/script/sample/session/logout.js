/**
 * @description função de envio de dados do formulário para realizar o logout
 * @returns false
 */
function logout() {

    Loader.show();

    // Realiza o requerimento ao servidor
    fetch("session/logout")
        .then(response => response.json())
        .then(function (response) {
            switch (response.status) {
                case 200: {
                    window.location.reload();
                    break;
                }
                default: {
                    Dialog.alert(response.msg);
                    break;
                }
            }
            Loader.hide();
        });
    return false;
}