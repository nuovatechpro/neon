/**
 * @description função de envio de dados do formulário para realizar o login
 * @returns false
 */
function acessar() {

    Loader.show();

    // Realiza o requerimento ao servidor
    fetch('session', {
        body: new FormData(document.getElementById('form-acesso')),
        method: 'post'
    })
        .then(response => response.json())
        .then(function (response) {
            switch (response.status) {
                case 200: {
                    window.location.reload();
                    break;
                }
                default: {
                    Dialog.alert(response.msg, 'Oops', function () {
                        Dialog.close(Dialog.key);
                    });
                    break;
                }
            }
            Loader.hide();
        })
        .catch(function (erro) {
            Dialog.alert(erro.getMessage());
        });
    return false;
}