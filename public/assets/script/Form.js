/**
 * @description Objeto de controle de formulários
 */
var Form = {

    /**
     * @description Realiza a validação do formato do arquivo anexado.
     * @param {string}  idContainer recebe o id do input html que contém o arquivo.
     * @param {Array}   mimeType recebe um array com os formatos permitidos para anexação.
     */
    attachment: function (idContainer, mimeType) {

        
        // Obtem o container do arquivo através do Id informado.
        var container = document.getElementById(idContainer);

        console.log(container);

        // Verifica se o container possui um valor.
        if (container.value.trim() != '') {

            // Validação do formato do arquivo pelos mimeTypes informados.
            for (let index = 0; index < mimeType.length; index++) {

                if ((container.value).includes('.' + mimeType[index]) == true) {
                    console.log("validado");
                    return true;
                }
            }

        } else {
            console.log("valiadasdado");
            console.warn("Form.attachment(): o valor do container está vazio!");
            return false;
        }
    },


    /**
     * @description gerenciamento de mensagem de erro para formulário
     */
    error: {

        /**
         * @description Realiza a remoção das caixas de erro
         */
        clear: function () {
            $(".form__error").remove();
        },


        /**
         * @param {string} bind id do elemento de referência
         * @param {string} msg mensagem que será exibida na caixa de texto
         */
        show: function (bind, msg) {
            
            if (($("#errorId-" + bind)).length == 0) {

                // Valida se a variÃ¡vel de mensagem possui algum valor, caso contrÃ¡rio cria um texto alternativo
                msg = (msg == undefined || msg == '') ? "Preencha o campo " + bind + "!" : msg;

                // Obtem o fieldset pai
                var parentEl = ($("#" + bind)).parent();

                // Cria o elemento Span onde serÃ¡ adicionado o texto
                var spanError = document.createElement("span");
                $(spanError).addClass("form__error").attr("id", "errorId-" + bind).css({
                    "color": "red",
                    "display": "block",
                    "font-size": "0.8em",
                    "padding": "10px 0px"
                }).text(msg);

                // Adiciona o elemento no pai
                $(parentEl).append(spanError);
                $("#errorId-" + bind).animate({
                    duration: 300,
                    height: "easeOutBounce",
                    opacity: 1,
                });
            }
        }
    }
}
