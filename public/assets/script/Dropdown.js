var Dropdown = {
    closeAll: function () {
        let dropWindows = document.getElementsByClassName('neon-dropdown-window');
        for (let i = 0; i < dropWindows.length; i++) {
            let dropWindow = dropWindows[i];
            if (dropWindow.classList.contains('--active')) {
                dropWindow.classList.remove('--active');
            }
        }
    },

    toggle: function (id) {

        let dropWindow = document.getElementById(id);
        dropWindow.classList.toggle("--active");
    },
}