<?php

use \Neon\Core\View;

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta content="initial-scale=1.0" name="viewport">
    <meta content="Eduardo Marinho" name="author">

    <title><?php echo View::$global['site']; ?></title>

    <!-- IMPORTAÇÃO DO FAVICON -->
    <?php View::favicon('favicon', 'png'); ?>

    <!-- IMPORTAÇÃO DO CSS -->
    <?php
    View::css('core');
    ?>

    <!-- IMPORTAÇÃO DO SCRIPT -->
    <?php
    View::script("Dialog");
    View::script("Loader");
    View::script("Modal");
    ?>
</head>

<body id="neon">
    <header class="neon__header">
        <img src="public/assets/imgs/Nuovatech_Logo_Original_Preto.png">
    </header>
    <main class="neon__main">
        <?php View::getBody(); ?>
    </main>
    <footer class="neon__footer">
        <span>Nuovatech <?php echo date('Y'); ?>. Todos os direitos reservados.</span>
    </footer>
</body>

</html>