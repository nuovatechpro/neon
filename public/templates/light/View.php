<?php

namespace Template\Light;

use Directory;

/**
 * Classe de gerenciamento do template
 */
abstract class View
{
    /**
     * Armazena o conteúdo da visão
     * @var string
     */
    static private $body;

    /**
     * Armazena um array com as variáveis utilizadas no controle da visão.
     * @var array
     */
    static private $global;

    public static function template(string $path, array $vars = [], string $type = null)
    {

        // Verifica a extenssão do arquivo que será carregado, o padrão é .php        
        $extension = ($type == null) ? ".php" : '.' . $type;
        $view = "public/pages/" . $path . $extension;

        // Carrega o layout
        $layout = "public/template/layout.php";

        // Atribui os parâmetros para a variável global
        self::$global = $vars;

        // limpa a variável
        unset($vars);

        // Atribui a carga do conteúdo à variável global
        self::$body  = file_exists($view) ? file_get_contents($view) : '';

        // Verica se o conteúdo está vazio, caso esteja carrega a página de erro.
        if (empty(self::$body)) {
            self::$body = file_get_contents("public/pages/status/error-view.php");
            self::$global = [
                "msg" => "Página não encontrada",
                "site" => "Neon:: 404 - Página não encontrada.",
                "status" => 404,
            ];
        }

        ob_start();
        eval("?>" . self::$body  . "<?php");
        self::$body  = ob_get_clean();
        require $layout;
    }
}
