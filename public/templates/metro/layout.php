<?php

// Importação da classe Sessão do Framework
use \Neon\Core\Session;

// Importação das classes de gestão do template
use \Template\Metro\Src\Navigation;
use \Template\Metro\Src\View;

?>

<!DOCTYPE html>

<html lang="pt-br">

<head>

    <meta charset="UTF-8">
    <meta content="Nuovatech" name="author">
    <meta content="" name="">
    <meta content="" name="keywords">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title><?php View::getGlobal('site'); ?></title>

    <!-- Importação do ícone do template. -->
    <?php View::favicon("favicon", 'ico'); ?>

    <!-- Importação dos scripts do template. -->
    <?php View::module("Jquery"); ?>
    <?php View::script("Application"); ?>
    <?php View::script("layout", "template"); ?>
    <?php View::module("Loader"); ?>
    <?php View::module("Modal"); ?>
    <?php View::module("Dialog"); ?>

    <!-- Importação das variáveis globais do template. -->
    <?php View::css("root"); ?>

    <!-- Importação do css do template -->
    <?php View::css("core", "template"); ?>

    <style>
        @media all {

            #btnMenu {
                background-color: transparent !important;
                border: 0 !important;
                cursor: pointer;
                filter: opacity(80%);
                height: 42px;
                width: 42px;
            }

            #btnMenu:hover {
                transition: ease-in-out 0.3s;
                filter: opacity(100%);
            }

            .window-header,
            .window-body,
            .window-footer {
                display: flex;
                flex-direction: row;
                padding: 10px;
            }

            .neon-dropdown-window .window-header {
                border-top-left-radius: .25rem !important;
                border-top-right-radius: .25rem !important;
            }

            .neon-dropdown-window .window-body {}

            .neon-dropdown-window .window-footer {
                border-bottom-left-radius: .25rem !important;
                border-bottom-right-radius: .25rem !important;
            }
        }

        @keyframes fade-in2 {
            0% {
                margin-top: -50px;
                visibility: hidden;
                opacity: 0;
            }

            100% {
                margin-top: 0;
                visibility: visible;
                opacity: 1;
            }
        }
    </style>

    <script>
        function menu() {
            let menu = document.getElementById('menu-navigation');
            menu.classList.toggle("show");
        }
    </script>
</head>

<body class="metro-template">

    <!-- Área destinada a implementação do controle da aplicação. -->
    <header class="metro-template-header">

        <!-- Call to action para o menu. -->
        <div class="metro-template-header-area">
            <button id="metro-template-menu" onclick="Metro.Navigation.toggle(this);" title="Menu de navegação" type="button">
                <?php View::svg('menu', 18, '#000'); ?>
            </button>
        </div>

        <div class="metro-template-header-area">
        </div>

        <!-- Call to action para o menu de usuário. -->
        <div class="metro-template-header-area">
            <div class="neon-dropdown">
                <button id="metro-template-menu-user" onclick="" title="Menu de navegação" type="button">
                    <img alt="avatar do usuário" src="<?php View::getGlobal('avatar'); ?>">
                </button>
                <!-- <div class="neon-dropdown-window --right" id="menu-usuario">
                    <div class="window-header">
                        <div class="">
                            <figure>
                                <img src="https://demo.dashboardpack.com/architectui-html-pro/assets/images/avatars/1.jpg">
                            </figure>
                        </div>
                        <div class="">
                            <span class="">
                              
                            </span>
                            <span class="">
                                Administrador
                            </span>
                        </div>
                    </div>
                    <div class="window-body"></div>

                    <div class="window-footer text-align --center">
                        <button class="neon-btn btn__size --mid btn__shape --smooth" onclick="Usuario.logout();" type="button">Sair</button>
                    </div>
                </div> -->
            </div>

        </div>

    </header>

    <!-- Área destinada a implementação do conteúdo principal. -->
    <main class="metro-template-main">

        <!-- Área destinada a implementação do menu de navegação lateral. -->
        <aside class="metro-template-aside metro-template-aside-side-menu show" id="metro-template-aside-menu">
            <?php Navigation::render(); ?>
        </aside>

        <!-- Área destinada a implementação do conteúdo das páginas. -->
        <aside class="metro-template-aside metro-template-aside-content">
            <?php echo View::getBody(); ?>
        </aside>
    </main>

</body>

</html>