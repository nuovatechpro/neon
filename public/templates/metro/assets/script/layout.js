/**
 * @description Objeto de controle do template
 * @author Eduardo Marinho
 */
const Metro = {

    Navigation: {

        submenu: function (option) {
            let navOpt = option.parentNode;
            navOpt.classList.toggle('--show');
        },

        toggle: function (btn) {
            let menu = document.getElementById("metro-template-aside-menu");
            menu.classList.toggle('show');
        },
    },
}