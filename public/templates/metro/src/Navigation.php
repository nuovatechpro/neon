<?php

namespace Template\Metro\Src;

use \Neon\Core\Neon;
use stdClass;

/**
 * Classe de construção da navegação lateral
 */
abstract class Navigation
{
    /**
     * Armazena um array contendo a construção da navegação
     * @var array
     */
    private static $navigation = [];

    /**
     * Método responsável por definir as opções da navegação
     * @param   array $navigation array contendo informações da navegação.
     */
    public static function setNavigation(array $navigation = [])
    {

        // Percorre o array para criar as opções
        foreach ($navigation as $option) {

            $navigation = new stdClass();
            $navigation->id = $option['id'];
            $navigation->idMenu = (isset($option['id_menu'])) ? $option['id_menu'] : null;
            $navigation->icon = (isset($option['icon'])) ? $option['icon'] : null;
            $navigation->label = $option['label'];
            $navigation->name = $option['name'];
            $navigation->title = isset($option['title']) ? $option['title'] : $option['name'];
            $navigation->url  = ($option['url'] == '#') ? '#' : self::link($option['url']);
            $navigation->qtdSubMenu = $option['qtd_sub_menu'];

            array_push(self::$navigation, $navigation);
        }
    }

    public static function render()
    {
        $listItem = '';
        foreach (self::$navigation as $option) {

            // Verifica se possui idmenu associado, caso não possua idetifica o elemento como pai
            if (empty($option->idMenu)) {

                // Cria o link para adicionar no objeto de lista
                if ($option->qtdSubMenu > 0) {
                    $link = "<a class='metro-navigation-parent' href='$option->url' onclick='Metro.Navigation.submenu(this); ' title='$option->title'>";
                } else {
                    $link = "<a href='$option->url' onclick='Loader.show();' title='$option->title'>";
                }

                // Verifica se possui ícone, caso sim, adiciona a configuração
                if (isset($option->icon)) {
                    $link .= View::svg($option->icon, 18, '#fff', true);
                }

                $link .= $option->label;

                foreach (self::$navigation as $a) {
                    if ($a->idMenu == $option->id) {
                        $link .= View::svg('arrow-down', 18, '#fff', true);
                        break;
                    }
                }

                $body = "<div class='container'>";

                foreach (self::$navigation as $sub) {



                    if ($sub->idMenu == $option->id) {

                        $link .= "<a class='metro-navigation-child' href='$sub->url' onclick='Loader.show();' title='$sub->title'>";
                        $link .= $sub->label;

                        // Verifica se possui ícone, caso sim, adiciona a configuração
                        if (isset($sub->icon)) {
                            $link .= View::svg($sub->icon, 18, '#fff', true);
                        }

                        $link .= "</a>";
                    }
                }

                $link .= "</a>";

                if ($option->qtdSubMenu > 0) {
                    $listItem .= "<li class='metro-navigation-parent'>$link</li>";
                } else {
                    $listItem .= "<li>$link</li>";
                }
            }
        }
        echo "<nav class='metro-navigation'><ul>$listItem</ul></nav>";
    }

    /**
     * Método de retorno da url absoluta da aplicação
     * @param   string $url, complemento do endereço
     */
    static private function link($url = null)
    {
        // Obtem o endereço da raíz do index
        $urix = explode('/', $_SERVER['PHP_SELF']);

        // Remove o index.php do array
        unset($urix[count($urix) - 1]);

        // Remonta o caminho utilizando a URL editada
        return $_SERVER['REQUEST_SCHEME'] . "://" .  $_SERVER['SERVER_NAME'] . implode('/', $urix) . '/' . $url;
    }
}
